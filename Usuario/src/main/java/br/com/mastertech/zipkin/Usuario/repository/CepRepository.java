package br.com.mastertech.zipkin.Usuario.repository;

import br.com.mastertech.zipkin.Usuario.model.Cep;
import org.springframework.data.repository.CrudRepository;

public interface CepRepository extends CrudRepository<Cep, Integer> {
}
