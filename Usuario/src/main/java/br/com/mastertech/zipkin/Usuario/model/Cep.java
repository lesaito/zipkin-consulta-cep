package br.com.mastertech.zipkin.Usuario.model;

import javax.persistence.*;

@Entity
@Table
public class Cep {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "cep_id")
    private int id;

    @Column
    private String cep;

    @Column
    private String complemento;

    @Column
    private String bairro;

    @Column
    private String uf;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }
}
