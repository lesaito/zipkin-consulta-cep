package br.com.mastertech.zipkin.Usuario.controller;

import br.com.mastertech.zipkin.Usuario.client.dto.CepDto;
import br.com.mastertech.zipkin.Usuario.model.Cep;
import br.com.mastertech.zipkin.Usuario.model.Dto.UsuarioRequest;
import br.com.mastertech.zipkin.Usuario.model.Usuario;
import br.com.mastertech.zipkin.Usuario.service.CepService;
import br.com.mastertech.zipkin.Usuario.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    CepService cepService;

    @Autowired
    UsuarioService usuarioService;

    @PostMapping("/registra")
    @ResponseStatus(HttpStatus.OK)
    public Usuario registrar(@RequestBody UsuarioRequest usuarioRequest) {
        CepDto cepDto = null;
        cepDto = cepService.buscaCep(usuarioRequest.getCep());
        if (cepDto != null) {
            Cep cep = new Cep();
            cep.setCep(cepDto.getCep());
            cep.setBairro(cepDto.getBairro());
            cep.setComplemento(cepDto.getComplemento());
            cep.setUf(cepDto.getUf());
            cepService.salva(cep);

            Usuario usuario = new Usuario();
            usuario.setNome(usuarioRequest.getNome());
            usuario.setCep(cep);
            return usuarioService.salvaUsuario(usuario);

        } else {
            throw new RuntimeException("Cep não encontrado");
        }
    }
}
