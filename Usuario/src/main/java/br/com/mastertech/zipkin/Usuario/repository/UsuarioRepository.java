package br.com.mastertech.zipkin.Usuario.repository;

import br.com.mastertech.zipkin.Usuario.model.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
}
