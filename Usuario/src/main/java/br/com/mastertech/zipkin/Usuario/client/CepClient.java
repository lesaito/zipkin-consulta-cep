package br.com.mastertech.zipkin.Usuario.client;

import br.com.mastertech.zipkin.Usuario.client.dto.CepDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep")
public interface CepClient {

    @GetMapping("/cep/{cep}")
    public CepDto buscaCep(@PathVariable String cep);
}
