package br.com.mastertech.zipkin.Usuario.service;

import br.com.mastertech.zipkin.Usuario.client.CepClient;
import br.com.mastertech.zipkin.Usuario.client.dto.CepDto;
import br.com.mastertech.zipkin.Usuario.model.Cep;
import br.com.mastertech.zipkin.Usuario.repository.CepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    CepRepository cepRepository;

    @Autowired
    CepClient cepClient;

    public CepDto buscaCep(String cep) {
        CepDto cepDto = null;
        try {
            cepDto = cepClient.buscaCep(cep);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cepDto;
    }

    public Cep salva(Cep cep) {
        return cepRepository.save(cep);
    }
}
