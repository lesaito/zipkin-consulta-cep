package br.com.mastertech.zipkin.Usuario.service;

import br.com.mastertech.zipkin.Usuario.model.Usuario;
import br.com.mastertech.zipkin.Usuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public Usuario salvaUsuario (Usuario usuario) {
        return usuarioRepository.save(usuario);
    }
}
