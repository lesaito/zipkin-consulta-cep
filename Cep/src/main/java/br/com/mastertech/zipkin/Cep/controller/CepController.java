package br.com.mastertech.zipkin.Cep.controller;

import br.com.mastertech.zipkin.Cep.client.dto.CepDto;
import br.com.mastertech.zipkin.Cep.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("cep")
public class CepController {

    @Autowired
    CepService cepService;

    @GetMapping("/{cep}")
    @ResponseStatus(HttpStatus.OK)
    public CepDto buscaCep(@PathVariable String cep) {
        return cepService.buscaCep(cep);
    }
}
