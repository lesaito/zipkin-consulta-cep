package br.com.mastertech.zipkin.Cep.service;

import br.com.mastertech.zipkin.Cep.client.CepClient;
import br.com.mastertech.zipkin.Cep.client.dto.CepDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    CepClient cepClient;

    public CepDto buscaCep (String cep) {
        CepDto cepDto = null;
        try {
            cepDto = cepClient.buscarCep(cep);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cepDto;
    }
}
