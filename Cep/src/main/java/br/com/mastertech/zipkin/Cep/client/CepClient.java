package br.com.mastertech.zipkin.Cep.client;

import br.com.mastertech.zipkin.Cep.client.dto.CepDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "http://viacep.com.br")
public interface CepClient {

    @GetMapping("/ws/{cep}/json")
    public CepDto buscarCep(@PathVariable String cep);

}
