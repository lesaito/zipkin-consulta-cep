# Exercício de Zipkin - Request Tracing
Construa 2 microsserviços:
O primeiro deve ter um endpoint que consulta CEPs no ViaCep
O segundo deve chamar o primeiro microsserviço e criar um usuário com o resultado do CEP.
Você deve acessar esses microsserviços somente via Zuul.
Obs: Todos os microsserviços que você usar devem ter o Zipkin instalado.

comando rodar dashboard 
docker run -d -p 9411:9411 openzipkin/zipkin
sudo systemctl start docker
